﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScreenSaver
{
    class ProcessIcon : Form
    {
        private static NotifyIcon trayIcon;
        private ContextMenu trayMenu;

        public ProcessIcon()
        {
            trayIcon = new NotifyIcon();
            createMenu();
        }

        public void Display()
        {
            trayIcon.Text = "PixelSaver";
            Bitmap b = new Bitmap(ScreenSaver.Properties.Resources.icon);
            IntPtr pIcon = b.GetHicon();
            Icon icon = Icon.FromHandle(pIcon);
            trayIcon.Icon = icon;
            trayIcon.Visible = true;
        }

      

        private MenuItem pauseItem;
        private MenuItem resumeItem;

        public object Interaction { get; private set; }

        private void createMenu()
        {
            trayMenu = new ContextMenu();
            pauseItem = new MenuItem("Pause", onPause);
            pauseItem.DefaultItem = true;
            resumeItem = new MenuItem("Resume", onResume);
            resumeItem.DefaultItem = true;
            trayMenu.MenuItems.Add(pauseItem);
            trayMenu.MenuItems.Add("Settings", onSettings);
            trayMenu.MenuItems.Add("Exit", onExit);
            trayIcon.ContextMenu = trayMenu;
        }

        private void onPause(object sender, EventArgs e)
        {
            trayMenu.MenuItems.Remove(pauseItem);
            trayMenu.MenuItems.Add(0, resumeItem);
            Program.disable();
        }

        private void onResume(object sender, EventArgs e)
        {
            trayMenu.MenuItems.Remove(resumeItem);
            trayMenu.MenuItems.Add(0, pauseItem);            
            Program.enable();
        }

        private void onExit(object sender, EventArgs e)
        {
            Program.exit();
        }

        private void onSettings(object sender, EventArgs e)
        {
            SettingsPrompt prompt = new SettingsPrompt();
            prompt.ShowDialog();
                        
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScreenSaver
{
    class PixelateBlur : Blur
    {

        public static int pixelSize = 20;

        public override Image blur(Bitmap image)
        {
            Bitmap result = new Bitmap(image.Width, image.Height);

            for (int x = 0; x < image.Width; x += pixelSize)

            {

                for (int y = 0; y < image.Height; y += pixelSize)
                {


                    int accR = 0;
                    int accG = 0;
                    int accB = 0;
                    int accA = 0;



                    for (int i = x; i < x + pixelSize; i++)
                    {
                        if (i >= image.Width)
                            continue;
                        for (int j = y; j < y + pixelSize; j++)
                        {
                            if (j >= image.Height)
                                continue;
                            Color pixelColor = image.GetPixel(i, j);

                            accR += pixelColor.R;
                            accG += pixelColor.G;
                            accB += pixelColor.B;
                            accA += pixelColor.A;


                        }
                    }






                    float newR = accR / (pixelSize * pixelSize);
                    float newG = accG / (pixelSize * pixelSize);
                    float newB = accB / (pixelSize * pixelSize);
                    float newA = accA / (pixelSize * pixelSize);



                    for (int i = x; i < x + pixelSize; i++)
                    {
                        if (i >= image.Width)
                            continue;
                        for (int j = y; j < y + pixelSize; j++)
                        {
                            if (j >= image.Height)
                                continue;
                            result.SetPixel(i, j, Color.FromArgb((int)newA, (int)newR, (int)newG, (int)newB));

                        }
                    }
                }
            }
            return result;
        }
    }
}

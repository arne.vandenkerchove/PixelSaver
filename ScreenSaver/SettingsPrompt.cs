﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScreenSaver
{
    class SettingsPrompt
    {

       private static readonly int pixelSize_MAX = 500;

        private static readonly int timeout_MIN = 1;
        private static readonly int timeout_MAX= 60 * 5;

        
        private NumericUpDown timeoutBox;

        private NumericUpDown parameterBox;

        public void ShowDialog()
            {
                Form prompt = new Form()
                {
                    Width = 200,
                    Height = 300,
                    FormBorderStyle = FormBorderStyle.FixedDialog,
                    Text = "Settings",
                    StartPosition = FormStartPosition.CenterScreen
                };

            //    Label blurModeLabel = new Label() { Left = 20, Top = 20, Text = "Blur mode:" };
            //    ComboBox blurModeBox = new ComboBox() { Left = 20, Top = 50, Width = 150 };

            //blurModeBox.DropDownStyle = ComboBoxStyle.DropDownList;

            //blurModeBox.Items.Add("Pixelate");
            //blurModeBox.Items.Add("Gaussian");
            //blurModeBox.Items.Add("Downscale");

            //blurModeBox.SelectedIndex = 0;

            //blurModeBox.SelectedIndexChanged += new EventHandler(updateParameterBox);

            //prompt.Controls.Add(blurModeBox);
            //    prompt.Controls.Add(blurModeLabel);

            Label pixelSizeLabel = new Label() { Left = 20, Top = 20, Text = "Pixel size:" };
            parameterBox = new NumericUpDown() { Left = 20, Top = 50, Width = 50 };
            parameterBox.Minimum = 5;
            parameterBox.Maximum = pixelSize_MAX;
            parameterBox.Value = PixelateBlur.pixelSize;

            prompt.Controls.Add(parameterBox);
            prompt.Controls.Add(pixelSizeLabel);

            Label timeoutLabel = new Label() { Left = 20, Top = 100, Text = "Timeout (min):" };
                timeoutBox = new NumericUpDown() { Left = 20, Top = 130, Width = 50 };
                timeoutBox.Minimum = timeout_MIN;
                timeoutBox.Maximum = timeout_MAX;
                timeoutBox.Value = Convert.ToInt32(Math.Ceiling((double)(Program.waitTime / (60 * 1000)))); 

                prompt.Controls.Add(timeoutBox);
                prompt.Controls.Add(timeoutLabel);

                Button preview = new Button() { Text = "Preview", Left = 20, Width = 100, Top = 190 };
                preview.Click += (sender, e) => { apply(); Program.ShowScreenSaver(); };
                prompt.Controls.Add(preview);

            Button confirmation = new Button() { Text = "Ok", Left = 20, Width = 100, Top = 220, DialogResult = DialogResult.OK };
                confirmation.Click += (sender, e) => { prompt.Close(); };
                
                prompt.Controls.Add(confirmation);                
                prompt.AcceptButton = confirmation;

            if (prompt.ShowDialog() == DialogResult.OK)
                apply();

            prompt.Visible = false;
            prompt.Close();
            prompt.Dispose();
                         
        }

        private void updateParameterBox(Object sender, EventArgs args)
        {
            Console.Write("changed");
        }

        private void apply()
        {
            PixelateBlur.pixelSize = Convert.ToInt32(parameterBox.Value);
            Program.waitTime = Convert.ToInt32(timeoutBox.Value * (60 * 1000));
        }

       }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScreenSaver
{
    

    class DownscaleBlur : Blur
    {
        public static int scale = 20;

        public override Image blur(Bitmap image)
        {
            Size size = new Size(image.Width / scale, image.Height / scale);
            Bitmap downscaled = new Bitmap(image, size);
            Bitmap upscaled = new Bitmap(downscaled, image.Size);
            return upscaled;
        }
    }
}

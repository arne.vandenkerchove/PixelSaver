﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScreenSaver
{
    abstract class Blur
    {
        public abstract Image blur(Bitmap i);

    }
}

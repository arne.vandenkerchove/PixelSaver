﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using System.Threading;
using System.Runtime.InteropServices;
using System.Windows.Input;

namespace ScreenSaver
{
    public partial class ScreenSaverForm : Form
    {
        private Point lastCursorPosition;

        public ScreenSaverForm()
        {
            InitializeComponent();
           
        }

        public ScreenSaverForm(Rectangle Bounds, Image image)
        {

            //this.Cursor = new Cursor(ScreenSaver.Properties.Resources.transparent.GetHbitmap());
            InitializeComponent();
            this.Opacity = 0;
            StartPosition = FormStartPosition.Manual;
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            SetBounds(Bounds.X, Bounds.Y, Bounds.Width, Bounds.Height);
            this.BackgroundImage = image;
            this.Dock = DockStyle.Fill;
            this.lastCursorPosition = Cursor.Position;
            Cursor.Hide();
            Cursor.Position = new Point(Bounds.Right, Bounds.Top);

        }

        public void ShowFade()
        {            
            this.Show();
            this.Focus();
            this.FadeIn();        
           
        }
        private bool fading = false;

        private async void FadeIn(int interval = 40)
        {
            fading = true;
            //Object is not fully invisible. Fade it in
            while (this.Opacity < 1.0)
            {
                await Task.Delay(interval);
                this.Opacity += 0.05;
            }
            this.Opacity = 1; //make fully visible  
            fading = false;
                 
        }

        private async void FadeOut(int interval = 40)
        {
            if (fading)
                return;

            //Object is fully visible. Fade it out
            while (this.Opacity > 0.0)
            {
                await Task.Delay(interval);
                this.Opacity -= 0.05;
            }
            this.Opacity = 0; //make fully invisible     

            Cursor.Show();
            Cursor.Position = this.lastCursorPosition;

            this.Visible = false;
            this.Close();
            this.Dispose();
            Program.showingForm = false;
            Program.restartTimer();
            
        }


        private void ScreenSaverForm_Load(object sender, EventArgs e)
        {
            TopMost = true;
        }

        private Point mouseLocation;

        private void ScreenSaverForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (!mouseLocation.IsEmpty)
            {
                // Terminate if mouse is moved a significant distance
                if (Math.Abs(mouseLocation.X - e.X) > 1 ||
                    Math.Abs(mouseLocation.Y - e.Y) > 1)
                {
                    FadeOut();
                }
            }

            // Update current mouse location
            mouseLocation = e.Location;
        }

        private void ScreenSaverForm_MouseClick(object sender, MouseEventArgs e)
        {
            FadeOut();
        }

        private void ScreenSaverForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            FadeOut();
            
        }

        protected override CreateParams CreateParams
        {
            get
            {
                // Activate double buffering at the form level.  All child controls will be double buffered as well.
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED
                return cp;
            }
        }

  

    }


}
